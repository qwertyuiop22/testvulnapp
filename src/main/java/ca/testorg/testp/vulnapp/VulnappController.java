package ca.testorg.testp.vulnapp;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class VulnappController {

    //should be GetMapping
    /*@RequestMapping("/")
    public String index() {
        return "index";
    }*/

    @GetMapping("/hello")
    public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "hello";
    }

    @GetMapping(value = "/login")
    public String login() {
        return "login";
    }

    /*@RequestMapping(value = "/login", method = RequestMethod.POST)
    public String postLogin() {
        // TODO Enable form login with Spring Security (trigger error for now)
        return "redirect:/login-error";
    }*/

    @GetMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "login";
    }

    //Mock directory traversal to linux passwd file
    /*@GetMapping("/etc/passwd")
    public String passwd(){
        return "smithj:x:561:561:Joe Smith:/home/smithj:/bin/bash";
    }

    //Mock directory traversal to linux shadow file
    @GetMapping("/etc/shadow")
    public String shadow(){
        return "smithj:Ep6mckrOLChF.:10063:0:99999:7:::";
    }*/

    //Real directory traversal
    //TODO: page qui lit un fichier sur le disque et l'affiche avec Java.io
    /*@RequestMapping("/file")
    public String postFile {
        String path = getInputPath();
        if (path.startsWith("/safe_dir/"))
        {
            File f = new File(path);
            f.delete()
        }
    }*/

    //Command injection
    //TODO: page qui lance un script avec argument, permet d'injecter du code
    /*
    ...
    String btype = request.getParameter("backuptype");
    String cmd = new String("cmd.exe /K \"
    c:\\util\\rmanDB.bat "
    +btype+
    "&&c:\\utl\\cleanup.bat\"")

    System.Runtime.getRuntime().exec(cmd);

    OR

    $userName = $_POST["user"];
    $command = 'ls -l /home/' . $userName;
    system($command);
    */

    //CWE-209: Information Exposure Through an Error Message
    //TODO: page qui output un message d'erreur dans la page web
    //new Exception().printStackTrace();
    /*
    try {
    /.../
    }
    catch (Exception e) {
    System.out.println(e);
    }
     */

    //CWE-532: Information Exposure Through Log Files
    //TODO: log credit card in clear text
    //logger.info("Username: " + usernme + ", CCN: " + ccn);


    //CWE-331: Insufficient Entropy
    /*String GenerateReceiptURL(String baseUrl) {
        Random ranGen = new Random();
        ranGen.setSeed((new Date()).getTime());
        return(baseUrl + ranGen.nextInt(400000000) + ".html");
    }*/

    //CWE-329: Not Using a Random IV with CBC Mode
    /*
    public class SymmetricCipherTest {
        public static void main() {

            byte[] text ="Secret".getBytes();
            byte[] iv ={
                    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
            };
            KeyGenerator kg = KeyGenerator.getInstance("DES");
            kg.init(56);
            SecretKey key = kg.generateKey();
            Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            IvParameterSpec ips = new IvParameterSpec(iv);
            cipher.init(Cipher.ENCRYPT_MODE, key, ips);
            return cipher.doFinal(inpBytes);
        }
    }*/

    //CWE-337: Predictable Seed in Pseudo-Random Number Generator (PRNG)
    /*
    Random random = new Random(System.currentTimeMillis());
    int accountID = random.nextInt();*/


    //CWE-759: Use of a One-Way Hash without a Salt
    /*String plainText = new String(plainTextIn);
    MessageDigest encer = MessageDigest.getInstance("SHA");
    encer.update(plainTextIn);
    byte[] digest = password.digest();
    //Login if hash matches stored hash
    if (equal(digest,secret_password())) {
        login_user();
        */

    //CWE-327: Use of a Broken or Risky Cryptographic Algorithm
    /*
    Cipher des=Cipher.getInstance("DES...");
    des.initEncrypt(key2);
    */


}